# Readme
Pour lire le rapport, il faut ouvrir le fichier HTML dans un navigateur.


# Objectif
On considère que la machine visu représente un citoyen C d'un pays X qui voudrait accéder à un film F (accessible via Camera) fournit par la plateforme P à laquelle il a souscrit à un forfait. Cependant, la plateforme P (Firewall) ne fournit ce film F qu'aux abonnés du pays Y.

Un founisseur d'accès VPN que nous sommes offrons la possibilité à C d'obtenir un accès au film F.